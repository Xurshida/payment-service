package uz.payment.util;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Util {

    public static final Short QUANTITY_LIMIT = 8;
    public static final Short PRODUCT_LIMIT = 10;
    public static final String SPECIFIC_DATE = "2016-09-06";
    public static final List<Map<String, Object>> nullArrayList = new ArrayList<>();
}
