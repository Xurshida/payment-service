package uz.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.payment.service.ProductService;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/list")
    public HttpEntity<?> getAll(){
        return ResponseEntity.ok(productService.getAll());
    }

    @GetMapping("/product/details")
    public HttpEntity<?> getProductDetails(@RequestParam Integer product_id){
        return ResponseEntity.ok(productService.getProductDetails(product_id));
    }

    @GetMapping("/high_demand_products")
    public HttpEntity<?> getHighDemandProducts(){
        return ResponseEntity.ok(productService.getProductsOrderedMoreThan10Times());
    }
    @GetMapping("/bulk_products")
    public HttpEntity<?> getBulkProducts(){
        return ResponseEntity.ok(productService.getBulkProducts());
    }
}
