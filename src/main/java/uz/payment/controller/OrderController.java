package uz.payment.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.payment.payload.OrderReq;
import uz.payment.service.OrderService;

@RestController
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/order")
    public HttpEntity<?> makeOrder(@RequestBody OrderReq orderReq) {
        return ResponseEntity.ok(orderService.makeOrder(orderReq));
    }

    @GetMapping("/order/details")
    public HttpEntity<?> getOrderDetails(@RequestParam Integer order_id) {
        return ResponseEntity.ok(orderService.getOrderDetails(order_id));
    }

    @GetMapping("/orders_without_details")
    public HttpEntity<?> getOrdersWithoutDetails() {
        return ResponseEntity.ok(orderService.getOrdersWithoutDetails());
    }

    @GetMapping("/number_of_order_in_year") // changed product to order
    public HttpEntity<?> getOrdersInYear() {
        return ResponseEntity.ok(orderService.getOrdersInYear());
    }

    //this looks weird, how can an order be without an invoice, if the invoice is created right after order is made
    @GetMapping("/orders_without_invoices")
    public HttpEntity<?> getOrdersWithoutInvoices() {
        return ResponseEntity.ok(orderService.getOrdersWithoutInvoices());
    }
}
