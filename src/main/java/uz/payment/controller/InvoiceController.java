package uz.payment.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.payment.service.InvoiceService;


@RestController
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/expired_invoices")
    public HttpEntity<?> getExpiredInvoices() {
        return ResponseEntity.ok(invoiceService.getExpiredInvoices());
    }

    //this looks weird, how can an invoice issued date be before the order date, if the invoice is created right after order is made
    @GetMapping("/wrong_date_invoices")
    public HttpEntity<?> getWrongDateInvoices() {
        return ResponseEntity.ok(invoiceService.getWrongDateInvoices());
    }

    @GetMapping("/overpaid_invoices")
    public HttpEntity<?> getOverpaidInvoices() {
        return ResponseEntity.ok(invoiceService.getOverpaidInvoices());
    }

}
