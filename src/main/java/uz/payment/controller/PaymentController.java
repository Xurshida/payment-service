package uz.payment.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.payment.payload.PaymentReq;
import uz.payment.service.PaymentService;

@RestController
public class PaymentController {
    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/payment")
    public HttpEntity<?> makePayment(@RequestBody PaymentReq paymentReq){
        return ResponseEntity.ok(paymentService.makePayment(paymentReq));
    }

    @GetMapping("/payment/details")
    public HttpEntity<?> getPayment(@RequestParam Integer payment_id){
        return ResponseEntity.ok(paymentService.getPaymentDetails(payment_id));
    }
}
