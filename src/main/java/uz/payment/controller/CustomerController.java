package uz.payment.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.payment.service.CustomerService;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customers_without_orders")
    public HttpEntity<?> getCustomersWithoutOrders() {
        return ResponseEntity.ok(customerService.getCustomersWithoutOrders());
    }

    @GetMapping("/customers_last_orders")
    public HttpEntity<?> getCustomersLastOrders() {
        return ResponseEntity.ok(customerService.getCustomersLastOrders());
    }


}
