package uz.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.payment.service.CategoryService;

@RestController
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category/list")
    public HttpEntity<?> getAll(){
        return ResponseEntity.ok(categoryService.getAll());
    }

    @GetMapping("/category")
    public HttpEntity<?> getByProductId(@RequestParam Integer product_id){
        return ResponseEntity.ok(categoryService.getByProductId(product_id));
    }
}
