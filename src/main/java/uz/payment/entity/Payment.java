package uz.payment.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    @CreationTimestamp
    private Timestamp time;

    @Column(columnDefinition = "NUMERIC(8,2)", nullable = false)
    private Double amount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Invoice invoice;
}
