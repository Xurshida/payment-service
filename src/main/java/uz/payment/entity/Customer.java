package uz.payment.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Customer{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 14, nullable = false)
    private String name;

    @Column(length = 3, nullable = false)
    private String country;

    @Column(columnDefinition = "text")
    private String address;

    @Column(length = 50, nullable = false, unique = true)
    private String phone;
}
