package uz.payment.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 10)
    private String name;

    @Column(length = 20)
    private String description;

    @Column(columnDefinition = "NUMERIC(6,2)")
    private Double price;

    @Column(length = 1024)
    private String photo;

    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;
}
