package uz.payment.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(columnDefinition = "NUMERIC(8,2)", nullable = false)
    private Double amount;

    @Column()
    //can be null, bc as I understand issued date is set when the payment is made
    private Date issued;

    @Column(nullable = false)
    //I set due date 1 day after the Invoice is created
    private Date due;

    @OneToOne(fetch = FetchType.LAZY)
    private Order order;

}
