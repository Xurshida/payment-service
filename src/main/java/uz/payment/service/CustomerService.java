package uz.payment.service;

import org.springframework.stereotype.Service;
import uz.payment.entity.Customer;
import uz.payment.repository.CustomerRepository;
import uz.payment.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getCustomersWithoutOrders() {
        try{
            List<Customer> customers = customerRepository.findCustomersWithoutOrders("2016-01-01", "2017-01-01");
            return customers.isEmpty() ? new ArrayList<>() : customers;
        }catch (Exception e){
            return new ArrayList<>();
        }
    }

    public List<Map<String, Object>> getCustomersLastOrders() {
        try {
           return customerRepository.findCustomersLastOrders();
        }catch (Exception e){
            return Util.nullArrayList;
        }
    }
}
