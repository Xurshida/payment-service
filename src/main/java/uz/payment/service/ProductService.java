package uz.payment.service;

import org.springframework.stereotype.Service;
import uz.payment.entity.Product;
import uz.payment.repository.DetailRepository;
import uz.payment.repository.OrderRepository;
import uz.payment.repository.ProductRepository;
import uz.payment.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final DetailRepository detailRepository;

    public ProductService(ProductRepository productRepository, DetailRepository detailRepository) {
        this.productRepository = productRepository;
        this.detailRepository = detailRepository;
    }


    public List<Product> getAll() {
        try {
            return productRepository.findAll();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }
    }

    public Product getProductDetails(Integer product_id) {
        Optional<Product> product = productRepository.findById(product_id);
        return product.orElse(null);
    }

    public List<Map<String, Object>> getProductsOrderedMoreThan10Times(){
        try {
            return detailRepository.findProductsOrderedMoreThan10Times(Util.PRODUCT_LIMIT);
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Util.nullArrayList;
        }
    }

    public List<Map<String, Object>> getBulkProducts() {
        try {
            return productRepository.findBulkProducts(Util.QUANTITY_LIMIT);
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Util.nullArrayList;
        }
    }
}
