package uz.payment.service;

import org.springframework.stereotype.Service;
import uz.payment.payload.InvoiceModel;
import uz.payment.repository.InvoiceRepository;
import uz.payment.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceService(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    public List<InvoiceModel> getExpiredInvoices() {
        try {
            return invoiceRepository.findInvoicesByIssuedIsAfterDue().stream().map(invoice ->
                new InvoiceModel(invoice.getId(), invoice.getOrder().getId(), invoice.getAmount(),
                    invoice.getIssued().toString(), invoice.getDue().toString())).collect(Collectors.toList());
        }catch (Exception e){
            return new ArrayList<>();
        }
    }

    public List<Map<String, Object>> getWrongDateInvoices() {
        try {
            return invoiceRepository.findInvoicesByIssuedIsBeforeOrderDate();
        }catch (Exception e){
            return Util.nullArrayList;
        }
    }

    public List<Map<String, Object>> getOverpaidInvoices() {
        try {
            return invoiceRepository.findOverpaidInvoices();
        }catch (Exception e){
            return Util.nullArrayList;
        }
    }
}
