package uz.payment.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.payment.entity.Invoice;
import uz.payment.entity.Payment;
import uz.payment.entity.Product;
import uz.payment.payload.PaymentModel;
import uz.payment.payload.PaymentReq;
import uz.payment.payload.PaymentRes;
import uz.payment.repository.InvoiceRepository;
import uz.payment.repository.PaymentRepository;

import java.sql.Date;
import java.util.Optional;

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final InvoiceRepository invoiceRepository;

    public PaymentService(PaymentRepository paymentRepository, InvoiceRepository invoiceRepository) {
        this.paymentRepository = paymentRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @Transactional
    public PaymentRes makePayment(PaymentReq paymentReq) {
        PaymentRes paymentRes = new PaymentRes();
        try{
            Payment payment = new Payment();
            payment.setAmount(paymentReq.getAmount());
            payment.setInvoice(invoiceRepository.findById(paymentReq.getInvoice_id()).get());
            paymentRes.setPayment_status("SUCCESS");
            payment = paymentRepository.save(payment);
            //I assumed that the invoice is issued when payment is made
            Invoice invoice = invoiceRepository.getById(payment.getInvoice().getId());
            invoice.setIssued(new Date(new java.util.Date().getTime()));
            invoiceRepository.save(invoice);
            paymentRes.setPaymentDetails(new PaymentModel(
                payment.getId(),
                payment.getTime().toString(),
                payment.getAmount(),
                payment.getInvoice().getId()));
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            paymentRes.setPayment_status("FAILED");
            paymentRes.setPaymentDetails(null);
        }
        return paymentRes;
    }

    public PaymentModel getPaymentDetails(Integer paymentId) {
        Optional<Payment> payment = paymentRepository.findById(paymentId);
        return payment.map(value -> new PaymentModel(paymentId, value.getTime().toString(), value.getAmount(), value.getInvoice().getId())).orElse(null);
    }
}
