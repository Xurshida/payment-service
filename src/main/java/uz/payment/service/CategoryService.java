package uz.payment.service;

import org.springframework.stereotype.Service;
import uz.payment.entity.Category;
import uz.payment.entity.Product;
import uz.payment.repository.CategoryRepository;
import uz.payment.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    public CategoryService(CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    public List<Category> getAll(){
        return categoryRepository.findAll();
    }

    public Category getByProductId(Integer product_id) {
        Optional<Product> product = productRepository.findById(product_id);
        return product.map(Product::getCategory).orElse(null);
    }
}
