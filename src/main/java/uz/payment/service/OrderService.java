package uz.payment.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.payment.entity.*;
import uz.payment.payload.DetailRes;
import uz.payment.payload.OrderDetails;
import uz.payment.payload.OrderReq;
import uz.payment.payload.OrderRes;
import uz.payment.repository.*;
import uz.payment.util.Util;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final DetailRepository detailRepository;
    private final InvoiceRepository invoiceRepository;

    public OrderService(OrderRepository orderRepository, CustomerRepository customerRepository, ProductRepository productRepository, DetailRepository detailRepository, InvoiceRepository invoiceRepository) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.detailRepository = detailRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @Transactional
    public OrderRes makeOrder(OrderReq orderReq) {
        OrderRes orderRes = new OrderRes();
        Order order = new Order();
        Invoice invoice = new Invoice();
        Detail detail = new Detail();
        try {
            Product product = productRepository.getById(orderReq.getProduct_id());
            Optional<Order> optionalOrder = orderRepository.findOrderByCustomerIdAndDate(orderReq.getCustomer_id(), new Date(new java.util.Date().getTime()));
            if (optionalOrder.isPresent()){
                order = optionalOrder.get();
                invoice = invoiceRepository.findInvoiceByOrder(order);
                invoice.setAmount(invoice.getAmount() + product.getPrice()*orderReq.getQuantity());
                invoice = invoiceRepository.save(invoice);
            }else {
                order.setDate(new Date(new java.util.Date().getTime()));
                order.setCustomer(customerRepository.getById(orderReq.getCustomer_id()));
                order = orderRepository.save(order);
                invoice.setOrder(order);
                invoice.setAmount(product.getPrice()*orderReq.getQuantity());
                invoice.setDue(new Date(new java.util.Date().getTime() + TimeUnit.DAYS.toMillis(1)));
                invoice = invoiceRepository.save(invoice);
            }
            detail.setOrder(order);
            detail.setProduct(productRepository.getById(orderReq.getProduct_id()));
            detail.setQuantity(orderReq.getQuantity());
            detailRepository.save(detail);
            orderRes.setStatus("SUCCESS");
            orderRes.setInvoice_number(invoice.getId());
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            orderRes.setStatus("FAILED");
            orderRes.setInvoice_number(null);
        }
        return orderRes;
    }

    public OrderDetails getOrderDetails(Integer orderId){
        OrderDetails orderDetails = new OrderDetails();
        Optional<Order> order = orderRepository.findById(orderId);
        if (order.isPresent()){
            orderDetails.setId(order.get().getId());
            orderDetails.setDate(order.get().getDate().toString());
            orderDetails.setCustomerId(order.get().getCustomer().getId());
            List<DetailRes> detailResList = detailRepository.getDetailsByOrderId(orderId).stream().map(detail ->
                new DetailRes(detail.getProduct().getName(), detail.getQuantity())).collect(Collectors.toList());
            orderDetails.setDetails(detailResList);
            return orderDetails;
        }
        return null;
    }

    public List<OrderDetails> getOrdersWithoutDetails() {
        try {
            return orderRepository.findOrdersWithoutDetails(Util.SPECIFIC_DATE).stream().map(order ->
                new OrderDetails(order.getId(), order.getDate().toString(), order.getCustomer().getId(), null)).collect(Collectors.toList());
        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<Map<String, Object>> getOrdersInYear() {
        try {
            return orderRepository.findOrdersInYear("2016-01-01", "2017-01-01");
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Util.nullArrayList;
        }
    }

    public List<Map<String, Object>> getOrdersWithoutInvoices() {
        try {
            return orderRepository.findOrdersWithoutInvoice();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Util.nullArrayList;
        }
    }
}
