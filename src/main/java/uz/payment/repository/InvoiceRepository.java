package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.payment.entity.Invoice;
import uz.payment.entity.Order;

import java.util.List;
import java.util.Map;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {
    Invoice findInvoiceByOrder(Order order);

    @Query(value = "select * from invoice i where i.issued > i.due", nativeQuery = true)
    List<Invoice> findInvoicesByIssuedIsAfterDue();

    @Query(value = "select i.id as id, i.issued as issued, o.id as order_id, o.date as order_date" +
        " from invoice i left join orders o on o.id = i.order_id where i.issued < o.date", nativeQuery = true)
    List<Map<String, Object>> findInvoicesByIssuedIsBeforeOrderDate();

    @Query(value = "select * from (select i.id as invoice_number,\n" +
        "       (select sum(p.amount) from payment p where p.invoice_id = i.id group by i.id having count(p.id) > 1) - i.amount as reimbursement\n" +
        "from invoice i) as list where reimbursement is not null", nativeQuery = true)
    List<Map<String, Object>> findOverpaidInvoices();
}
