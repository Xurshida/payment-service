package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.payment.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
