package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.payment.entity.Customer;

import java.util.List;
import java.util.Map;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    boolean existsCustomerByPhone(String phone);

    @Query(value = "select * from customer ct where ct.id not in\n" +
        "(select c.id from customer c inner join orders o on c.id = o.customer_id\n" +
        "and o.date >= cast(:fromDate as DATE) and o.date < cast(:toDate as DATE))", nativeQuery = true)
    List<Customer> findCustomersWithoutOrders(@Param(value = "fromDate") String fromDate,
                                              @Param(value = "toDate") String toDate);

    @Query(value = "select c.id as customer_id,\n" +
        "       c.name as customer_name,\n" +
        "       to_char(o2.date, 'YYYY-MM-DD') as last_order \n" +
        "from customer c left join orders o2 on c.id = o2.customer_id\n" +
        "where o2.date = (select o.date from orders o where o.customer_id = c.id order by o.date desc limit 1);", nativeQuery = true)
    List<Map<String, Object>> findCustomersLastOrders();


}
