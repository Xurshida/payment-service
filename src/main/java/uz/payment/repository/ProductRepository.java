package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.payment.entity.Product;

import java.util.List;
import java.util.Map;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "select p.id as product_code, p.price as price from product p left join detail d on p.id = d.product_id where d.quantity>=:quantityLimit", nativeQuery = true)
    List<Map<String, Object>> findBulkProducts(@Param(value = "quantityLimit") Short quantityLimit);
}
