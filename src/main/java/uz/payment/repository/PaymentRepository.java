package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.payment.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

}
