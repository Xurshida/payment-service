package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.payment.entity.Detail;
import uz.payment.entity.Product;

import java.util.List;
import java.util.Map;

public interface DetailRepository extends JpaRepository<Detail, Integer> {
    List<Detail> getDetailsByProduct(Product product);
    List<Detail> getDetailsByOrderId(Integer orderId);

    @Query(value = "select d.product_id as product_code, sum(d.quantity) as total_times from detail d group by d.product_id having sum(d.quantity) > :productLimit", nativeQuery = true)
    List<Map<String, Object>> findProductsOrderedMoreThan10Times(@Param(value = "productLimit") Short productLimit);


}
