package uz.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.payment.entity.Order;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    Optional<Order> findOrderByCustomerIdAndDate(Integer customer_id, Date date);

    @Query(value = "select * from orders os where os.date < cast(:date as date) and os.id not in " +
        "(select o.id from orders o join detail d on o.id = d.order_id)", nativeQuery = true)
    List<Order> findOrdersWithoutDetails(@Param(value = "date") String date);

    @Query(value = "select country, count(*) as total_order from (select o.id, c.country as country, o.date as date from orders o\n" +
        "    left join customer c on o.customer_id = c.id) as list\n" +
        "where list.date >= cast(:fromDate as date) and list.date < cast(:toDate as date)\n" +
        "group by list.country", nativeQuery = true)
    List<Map<String, Object>> findOrdersInYear(@Param(value = "fromDate") String fromDate, @Param(value = "toDate") String toDate);

    @Query(value = "select o.id as order_id, o.date as order_date, sum(d.quantity * p.price) as total_price from orders o\n" +
        "     join detail d on o.id = d.order_id join product p on d.product_id = p.id\n" +
        "where not exists(select 1 from invoice i where o.id = i.order_id)\n" +
        "group by (o.id, o.date, o.customer_id)", nativeQuery = true)
    List<Map<String, Object>> findOrdersWithoutInvoice();
}
