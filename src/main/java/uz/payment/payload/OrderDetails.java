package uz.payment.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetails {
    private Integer id;
    private String date;
    private Integer customerId;
    private List<DetailRes> details;
}
