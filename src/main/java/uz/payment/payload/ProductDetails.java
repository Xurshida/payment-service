package uz.payment.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.payment.entity.Order;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDetails {
    private Integer productId;
    private Order order;
    private Short quantity;
}
