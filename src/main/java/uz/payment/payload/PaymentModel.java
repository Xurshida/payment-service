package uz.payment.payload;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentModel {
    private Integer id;
    private String time;
    private Double amount;
    private Integer invoice_id;
}
