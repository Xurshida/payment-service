package uz.payment.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceModel {
    private Integer id;
    private Integer order_id;
    private Double amount;
    private String issued;
    private String due;
}
