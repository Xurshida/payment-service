insert into category (name)
values ('appliances'),
       ('food'),
       ('fruit');


insert into customer (name, country, phone, address)
values ('John', 'usa', '123456765', null),
       ('Fred', 'uzb', '123456798', null),
       ('Xurshida', 'uzb', '123456792', null),
       ('George', 'ru', '123456789', null);

insert into product (category_id, description, name, photo, price)
values (1, null, 'dishwasher', null, 10.999),
       (1, null, 'freezer', null, 20.010),
       (2, null, 'bread', null, 1),
       (2, null, 'noodles', null, 4),
       (3, null, 'apple', null, 3),
       (3, null, 'apricot', null, 5);

insert into orders (date, customer_id)
values ('2016-01-31', 1),
       ('2017-01-01', 2),
       ('2015-01-12', 1),
       ('2021-12-16', 1),
       ('2021-12-16', 2);

insert into detail(order_id, product_id, quantity)
values (1, 3, 2),
       (1, 1, 1),
       (1, 5, 10),
       (2, 1, 1),
       (2, 5, 6),
       (5, 1, 1),
       (5, 2, 1);

insert into invoice(order_id, amount, issued, due)
values (1, 42.999, '2016-01-29', '2016-01-02'),
       (2, 28.999, '2017-01-01', '2017-01-01'),
       (4, 31.009, '2021-12-16', '2021-12-16');

insert into payment (amount, time, invoice_id)
VALUES (30, '2021-12-16 19:44:59.424000', 3),
       (10, '2021-12-16 20:11:06.318000', 3),
       (10, '2021-12-16 20:31:37.941000', 2);



